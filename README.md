
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrels

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrels is to provide functions helping analyze the
squirrels of Central Park

## Installation

You can install the development version of squirrels like so:

``` r
install.packages("squirrels")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(squirrels)
get_message_fur_color(primary_fur_color = "Cinnamon")
#> We will focus on Cinnamon squirrels
```
